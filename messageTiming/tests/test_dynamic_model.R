context('testing calling dymanic_model func from R in TTE module')
print(Sys.time())

library(data.table)
library(reticulate)
  
VAL_NO_DATA <- 999 

use_python("/usr/local/bin/python")

source_python(sprintf("%s/messageTiming/build_dynamic_model.py",homedir))

ints <- data.table(accountId=c(1002,1002,1002,1002,1002,1002,1002,1002,1002,1002,1002,1003,1003,1003,1003),
                   type=c('SEND', 'SEND', 'SEND', 'SEND', 'VISIT', 'TARGET', 'TARGET', 'SEND', 'VISIT', 'VISIT', 'TARGET',
                     'SEND', 'SEND', 'VISIT', 'TARGET'), 
                   date=c("2018-06-01", "2018-07-03", "2018-07-06", "2018-07-10", "2018-07-13", "2018-07-14", "2018-07-15",
                     "2018-07-19", "2018-07-21", "2018-07-26", "2018-07-29", "2018-07-02", "2018-07-10", "2018-07-13",
                     "2018-07-14"))
ints[, date := as.Date(date)]

dm <- build_dynamic_model(ints) 

print(dm)

expect_equal(dm[2, 'preSEND'], 32)
expect_equal(dm[2, 'preVISIT'], VAL_NO_DATA)
expect_equal(dm[3, 'TARGET'], 1.0)
expect_equal(dm[4, 'numS'], 4)
expect_equal(dm[5, 'numV'], 1)
expect_equal(dm[5, 'event'], 'V')
expect_equal(dm[5, 'TARGET'], 0.75)
expect_equal(dm[6, 'preSEND'], 9)
expect_equal(dm[6, 'preTARGET'], 4)
expect_equal(dm[6, 'pre2S'], 13)
expect_equal(dm[6, 'TARGET'], 1.0)

expect_equal(dm[7, 'TARGET'], 0.75)
expect_equal(dm[7, 'preVISIT'], 8)
expect_equal(dm[7, 'numT'], 2)

expect_equal(dm[8, 'pre2S'], 16)
expect_equal(dm[8, 'TARGET'], 0.75)
expect_equal(dm[10, 'preSEND'], 8)
expect_equal(dm[10, 'numT'], 0)
expect_equal(dm[11, 'TARGET'], 0.5)
expect_equal(dm[11, 'preVISIT'], VAL_NO_DATA)

