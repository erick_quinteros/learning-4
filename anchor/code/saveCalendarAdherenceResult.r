##########################################################
#
#
# aktana-learning Anchor estimates Aktana Learning Engines.
#
# description: save calendar Adherence calculation results
#
#
# created by : learning-dev@aktana.com
# updated by : learning-dev@aktana.com
#
# created on : 2019-06-20
# updated on : 2019-06-20
#
# Copyright AKTANA (c) 2019.
#
#
####################################################################################################################
saveRepCalendarAdherence <- function(result, RUN_UID, BUILD_UID, predictRundate) {
  
  flog.info("Enter saveRepCalendarAdherence Result")
  # add learningBuildUID
  result$learningRunUID <- RUN_UID
  result$learningBuildUID <- BUILD_UID
  result$runDate <- predictRundate
  # delete old data first
  dataAccessLayer.anchor.truncate.RepCalendarAdherence()
  # write result back to DB
  flog.info("save %s row of RepCalendarAdherence to Learning", dim(result)[1])
  dataAccessLayer.anchor.write.RepCalendarAdherence_l(result)
}

saveRepAccountCalendarAdherence <- function(result, RUN_UID, BUILD_UID, predictRundate) {
  
  flog.info("Enter saveRepAccountCalendarAdherence Result")
  # add learningBuildUID
  result$learningRunUID <- RUN_UID
  result$learningBuildUID <- BUILD_UID
  result$runDate <- predictRundate
  # delete old data first
  dataAccessLayer.anchor.truncate.RepAccountCalendarAdherence()
  # write result back to DB
  flog.info("save %s row of RepAccountCalendarAdherence to Learning", dim(result)[1])
  dataAccessLayer.anchor.write.RepAccountCalendarAdherence_l(result)
}