context('testing loadAnchorData.Interaction, loadAnchorData.repAccountAssignment, loadAnchorData.repTeamRep, loadAnchorData.accountDateLocationScores func defined in loadAnchorData script in ANCHOR module')
print(Sys.time())

# load library and source script
library(futile.logger)
library(Learning)
library(Learning.DataAccessLayer)
source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir))
# script to run test function
source(sprintf("%s/anchor/code/loadAnchorData.R",homedir))

# set up db connnection
dataAccessLayer.common.initializeConnections(dbuser, dbpassword, dbhost, dbname, port)

# call tests
test_that("test loadAnchorData.Interaction", {
  # reset mock data
  requiredMockDataList <- list() # use module own data
  requiredMockDataList_module <- list(pfizerusdev=c('Interaction','Facility','Rep','InteractionType','InteractionAccount'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList,requiredMockDataList_module,'anchor')
  # requried input
  startDate <- as.Date(readModuleConfig(homedir, 'anchor', 'loadDataStartDate'))
  endDate <- as.Date(readModuleConfig(homedir, 'anchor', 'loadDataEndDate'))
  # run funcs
  interactions_new <- loadAnchorData.Interaction(startDate, endDate)

  # tests cases
  expect_equal(dim(interactions_new),c(6230,9))
  load(sprintf('%s/anchor/tests/data/from_loadAnchorData_interaction_all.RData', homedir))
  expect_equal(interactions_new[order(interactions_new$interactionId),],interactions[order(interactions$interactionId),])
})

test_that("test loadAnchorData.repAccountAssignment", {
  # reset mock data
  requiredMockDataList <- list() # use module own data
  requiredMockDataList_module <- list(pfizerusdev=c('Rep'),pfizerusdev_stage=('RepAccountAssignment_arc'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList,requiredMockDataList_module,'anchor')
  # requried input
  predictRundate <- as.Date(readModuleConfig(homedir, 'anchor', 'rundate'))
  validReps <- loadAnchorData.validReps()
  # run funcs
  repAccountAssignments_new <- loadAnchorData.repAccountAssignment(predictRundate, validReps)

  # test cases
  expect_equal(dim(repAccountAssignments_new),c(1012,2))
  load(sprintf('%s/anchor/tests/data/from_loadAnchorData_repAccountAssignments.RData', homedir))
  setkey(repAccountAssignments,NULL)
  expect_equal(repAccountAssignments_new, repAccountAssignments)
})

test_that("test loadAnchorData.filterInteraction", {
  # requried input
  load(sprintf('%s/anchor/tests/data/from_loadAnchorData_interaction_all.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_loadAnchorData_repAccountAssignments.RData', homedir))
  interactions <- interactions[!is.na(latitude) & !is.na(longitude)]  # filter out the one does not have latitude and longitude
  interactions[,c("timeZoneId", "startDateTime"):=NULL]
  # run funcs
  interactions_filter_new <- loadAnchorData.filterInteraction(interactions, repAccountAssignments)

  # test cases
  expect_equal(dim(interactions_filter_new),c(5529,7))
  valid_interactions <- merge(interactions, repAccountAssignments, by=c('repId', 'accountId'), all=FALSE)
  # if any row is dopped from interactions_merged, the repAccountAssignment associated w/ the interaction is invalid (old)
  expect_equal(interactions_filter_new, valid_interactions)
})

test_that("test loadAnchorData.repTeamRep", {
  # reset mock data
  requiredMockDataList <- list() # use module own data
  requiredMockDataList_module <- list(pfizerusdev=c('Rep','RepTeamRep'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList,requiredMockDataList_module,'anchor')
  # run funcs
  repTeamRep_new <- loadAnchorData.repTeamRep()

  # tests cases
  expect_equal(dim(repTeamRep_new),c(0,2))
  load(sprintf("%s/anchor/tests/data/from_loadAnchorData_repTeamRep.RData",homedir))
  expect_equal(repTeamRep_new,repTeamRep)
})

test_that("test loadAnchorData.accountDateLocationScores", {
  # reset mock data
  requiredMockDataList <- list() # use module own data
  requiredMockDataList_module <- list(pfizerusdev_learning=c('AccountDateLocationScores'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList,requiredMockDataList_module,'anchor')
  # run funcs
  accountDateLocationScores_new <- loadAnchorData.accountDateLocationScores()

  # tests cases
  expect_equal(dim(accountDateLocationScores_new),c(3661,8))
  load(sprintf('%s/anchor/tests/data/from_loadAnchorData_accountDateLocationScores.RData', homedir))
  accountDateLocationScores$facilityDoWScore <- as.double(accountDateLocationScores$facilityDoWScore)
  accountDateLocationScores$facilityPoDScore <- as.double(accountDateLocationScores$facilityPoDScore)
  expect_equal(accountDateLocationScores_new[order(accountId, facilityId, dayOfWeek, periodOfDay),],
               accountDateLocationScores[order(accountId, facilityId, dayOfWeek, periodOfDay),])
})

test_that("test loadAnchorData.newRepList", {
  # required input
  load(sprintf('%s/anchor/tests/data/from_loadAnchorData_repAccountAssignments.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_loadAnchorData_INTERACTIONS_REP.RData', homedir))
  newRepList_new <- loadAnchorData.newRepList(INTERACTIONS_REP, repAccountAssignments)

  # tests cases
  expect_equal(length(newRepList_new),0)
  expect_equal(newRepList_new,data.table())
})

dataAccessLayer.common.closeConnections()
