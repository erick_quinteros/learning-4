install.packages("sparklyr")
library(sparklyr)


spark_install(version = "2.3.1")

# Paths to set for the deployment 
learning_package_path <- "~/Documents/learning/sparkLearningPackage/scala"
scalac_path <- "/usr/local/bin/scalac"
extension_jar_name <- "LearningExtension.jar"
complie_spark_version <- "2.3.1"
extension_jar_path <- paste(learning_package_path, "inst", "java", extension_jar_name, sep = "/")

setwd(learning_package_path)

# Compile the scala code and generate jar
sparklyr::compile_package_jars(spec = spark_compilation_spec(spark_version = complie_spark_version, spark_home = NULL,
                                                             scalac_path = scalac_path, 
                                                             scala_filter=".scala", jar_name = extension_jar_name))

# create new Sparklyr config
config <- spark_config()

# add a new default jar to load on connection, path or relative path to custom jar
config[["sparklyr.jars.default"]] <- c(extension_jar_path)

# connect with config parameter
sc <- spark_connect(master = "local", version = "2.3.1", config = config)

# Test
# invoke function inside jar using fully qualified name defined in the custom jar
test_df <- sparklyr::invoke_static(sc, "LearningSparkExtension.DataWrangler", "test_dropColumnsWithLessThanUnqiueValues", array(c("1", "2")))

print(test_df)