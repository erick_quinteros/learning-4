.sparkColTypeChange <- function(sc, jdf, colName, newType, firstCovertToLong=FALSE) {
  # convert to char column
  if (firstCovertToLong) {
    newCol <- sqlf(sc, "col")(colName) %>% invoke("cast","LONG") %>% invoke("cast",newType)
  } else {
    newCol <- sqlf(sc, "col")(colName) %>% invoke("cast",newType)
  }
  # concat
  new_jdf <- jdf %>%
    invoke("withColumn", colName, newCol)
  # return
  return(new_jdf)
}

sparkColTypeChange <- function(df, colName, newType, firstConvertToLong=FALSE) {
  return(jdfFuncWrapper(.sparkColTypeChange, df, colName, newType, firstConvertToLong))
}