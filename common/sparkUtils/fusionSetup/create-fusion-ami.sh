#!/bin/bash

################################################################################
# This shell script runs and automatically creates an AMI instance in the      #
# specified region, with the specified image name (AMI). It will install       #
# packages and dependencies from the shell script - `learning-installer.sh`    #
# present in the same directory as this file.                                  #
#                                                                              #
#                   MODULE: Learning Fusion                                    #
#                   CREATED BY: Anwar Shaikh                                   #
#                   CREATED AT: 18-SEP-2019                                    #
#                   UPDATED BY: Anwar Shaikh                                   #
#                   UPDATED AT: 25-SEP-2019                                    #
################################################################################

# Check the number of Arguments
if [[ $# -ne 2 ]]; then
    echo "Illegal number of parameters to the script"
    exit 2
fi

# Parse the Arguments
SCIRPT_VERSION="1.0"
REGION=$1
IMAGE_NAME=$2

echo "Started Execution Script Version = $SCRIPT_VERSION in $REGION region for image $IMAGE_NAME"

# Setup AWS Credentials
export AWS_ACCESS_KEY_ID="AKIAXRUJHASADTAL4LBE"
export AWS_SECRET_ACCESS_KEY="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"
export AWS_DEFAULT_REGION="$REGION"

# Step-1: Create an EC-2 instance
INSTANCE_ID=$(aws ec2 run-instances --image-id ami-0ff8a91507f77f867 \
 --count 1 --instance-type m3.large \
 --key-name learning-team-dev --region $REGION \
 --security-group-ids sg-05ea6fcf3d1ed4bbe \
 --user-data file://fusion_installer.sh \
 --query 'Instances[0].InstanceId' --output text)

echo "Initiated EC2 Instance with ID: $INSTANCE_ID"

# Step-2: Wait until the instance state is Running
INSTANCE_STATE='started'
while [ "$INSTANCE_STATE" != "running" ];
  do
    echo "Waiting for instance to start..."
    sleep 3
    INSTANCE_STATE=$(aws ec2 describe-instance-status --instance-id $INSTANCE_ID \
    --query 'InstanceStatuses[0].InstanceState.Name' --output text)
done

# Wait until all the packages are installed
echo "Dependency installation in progress, it might take a while..."
sleep 2700 # Waitng 45 mins for the installation

# Step-2: Create an AMI from the instance above
echo "Creating image.."
IMAGE_ID=$(aws ec2 create-image --no-dry-run --instance-id $INSTANCE_ID --name $IMAGE_NAME \
 --description "Image created by learning automated script version $SCRIPT_VERSION" \
 --query 'ImageId' --output text)

echo "Image Created"

# Step-3 : Shutting down the instance
echo "Shutting down the instance - $INSTANCE_ID ..."
aws ec2 stop-instances --instance-ids $INSTANCE_ID

echo "Exiting the script"

# This will return the AMI Image Id to the calling script/method
echo "$IMAGE_ID"
