
class Singleton:
    """
    A non-thread-safe helper class to ease implementing singletons.
    This should be used as a decorator -- not a metaclass -- to the
    class that should be a singleton.

    The decorated class can define one `__init__` function that
    takes only the `self` argument. Also, the decorated class cannot be
    inherited from. Other than that, there are no restrictions that apply
    to the decorated class.

    To get the singleton instance, use the `instance` method. Trying
    to use `__call__` will result in a `TypeError` being raised.

    """

    def __init__(self, decorated):
        self._decorated = decorated

    def instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a
        new instance of the decorated class and calls its `__init__` method.
        On all subsequent calls, the already created instance is returned.

        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)


@Singleton
class DatabaseConfig:
    """
    This is a singleton class holds the database configuration for current instance.
    """

    def set_config(self, host, user, password, port=3306, dse_db_name=None, learning_db_name=None, cs_db_name=None, stage_db_name=None):
        """

        :param host:
        :param user:
        :param password:
        :param port:
        :param dse_db_name:
        :param learning_db_name:
        :param stage_db_name:
        """
        self.host = host
        self.user = user
        self.password = password
        self.port = port
        self.dse_db_name = dse_db_name
        self.learning_db_name = learning_db_name
        self.cs_db_name = cs_db_name
        self.stage_db_name = stage_db_name
