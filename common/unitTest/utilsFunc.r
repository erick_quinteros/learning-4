readLogFile <- function(homedir, BUILD_UID, RUN_UID) {
  log_file_path <- sprintf('%s/builds/%s/log_%s.txt',homedir,BUILD_UID,RUN_UID)
  log_contents <- readLogFileBase(log_file_path)
  return (log_contents)
}

readScoreLogFile <- function(homedir, BUILD_UID, RUN_UID) {
  log_file_path <- sprintf('%s/builds/%s/log_Score_%s.txt',homedir,BUILD_UID,RUN_UID)
  log_contents <- readLogFileBase(log_file_path)
  return (log_contents)
}

readLogFileBase <- function(log_file_path) {
  ff <- file(log_file_path,'r')
  log_contents <- readLines(ff, warn=FALSE)
  close(ff)
  return (log_contents)
}

checkNumOfFilesInFolder <- function(filePath) {
  return (length(list.files(filePath)))
}