# read args passed from command line
args <- commandArgs(T)
if(length(args)==0){
    print("No arguments supplied.")
    if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
        print("Arguments supplied.")
        for(i in 1:length(args)){
          eval(parse(text=args[[i]]))
          print(args[[i]]);
    }
}

source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir))

# read config.json
readConfig(homedir)
dbname_filepath_match <- processConfig(dbname)

# set database
library(RMySQL)
drv <- dbDriver("MySQL")
# con to mysql to create database if not exists
con_db <- dbConnect(drv,user=dbuser,host=dbhost,port=as.numeric(port),password=dbpassword)
# database list to be rewritten
dir_list <- list.dirs(path=paste(homedir,'/common/unitTest/dbSetup',sep=''),full.names=FALSE,recursive=FALSE)
# loop through list
for (dir in dir_list) {
  # get db Name
  db <- dbname_filepath_match[[dir]]
  # create database if not exists
  print(sprintf('Create table for schema:%s...',db))
  SQL <- sprintf("DROP SCHEMA IF EXISTS %s;",db)
  dbClearResult(dbSendQuery(con_db, SQL))
  SQL <- sprintf("CREATE SCHEMA %s CHARACTER SET utf8 COLLATE utf8_unicode_ci;",db)
  dbClearResult(dbSendQuery(con_db, SQL))
  # read local data and write to db
  con <- dbConnect(drv,user=dbuser,host=dbhost,dbname=db,port=as.numeric(port),password=dbpassword)
  dirpath <- paste(homedir,'/common/unitTest/dbSetup/',dir,sep='')
  filesToWrite <- list.files(path=dirpath, full.names=TRUE)
  for (filepath in filesToWrite) {
    setupTable(con, filepath)
  }
  dbDisconnect(con)
}
# disconnect mysql conn
dbDisconnect(con_db)

# Rscript common/unitTest/dbSetup.R homedir="'"'/Users/jiajingxu/Documents/learning'"'"