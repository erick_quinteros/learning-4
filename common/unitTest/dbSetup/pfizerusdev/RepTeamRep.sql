CREATE TABLE `RepTeamRep` (
  `repTeamId` int(11) NOT NULL,
  `repId` int(11) NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`repTeamId`,`repId`),
  KEY `repTeamRep_repTeamId` (`repTeamId`),
  KEY `repTeamRep_repId` (`repId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci