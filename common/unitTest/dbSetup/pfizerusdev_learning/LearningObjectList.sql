CREATE TABLE `LearningObjectList` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listType` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `objectType` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `objectUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci