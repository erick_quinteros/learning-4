####################################################################################################################
#
#
# aktana- engagement estimates estimates Aktana Learning Engines.
#
# description: estimate the target engagement probabilities
#
# created by : marc.cohen@aktana.com
# updated by : wendong.zhu@aktana.com
#
# updated on : 2019-03-12
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################

# helper function to estimate the likelihood for a visit based on the ECDF function fn
#E <- function(fn,x) return(fn$estimate[which(abs(fn$eval.points-x)==min(abs(fn$eval.points-x)))])

# main entry into the estimation function
calculateEngagement <- function(sc, chl, useForProbability, interactions, suggestions)
{
    library(ks)
    library(data.table)
    library(foreach)
    library(doMC)
    library(sparklyr)
    library(dplyr)

    registerDoMC(numberCores)  # setup the number of cores to use
    
    flog.info("calculateEngagement for channel %s ...", chl)

    #events <- interactions[repActionTypeId==chl & date<=today]  # subset the interactions for the right channel and today date
    events <- interactions %>% filter(repActionTypeId == chl & date <= today)

    #events <- unique(events,by=c("accountId","repId","date"))   # no need to have multiple records with the same account-rep-date combo
    # events <- distinct(events, accountId, repId, date, .keep_all = TRUE)
    # events <- events %>% 
    #   sdf_with_sequential_id(id="tempId", from=1) %>%
    #   group_by(accountId, repId, date) %>% 
    #   filter(tempId==min(tempId, na.rm=TRUE)) %>% 
    #   ungroup() %>%
    #   select(-tempId)
    events <- events %>%
      distinct(accountId, repId, date) %>%
      mutate(repActionTypeId=chl, reaction="Touchpoint")  # add back the two columns dropped due to distinct
    flog.info("finish initial prepare events")

    #suggestions <- suggestions[repActionTypeId==chl & date<=today & date>=sugStartDate]  # subset the suggestions for channel
    suggestions <- suggestions %>% filter(repActionTypeId==chl & date<=today & date>=sugStartDate)

    #setnames(suggestions,c("Suggestion_External_Id_vod__c"),c("suggestion"))
    suggestions <- dplyr::rename(suggestions, suggestion = Suggestion_External_Id_vod__c)

    #suggestions$suggestion <- gsub("AKTSUG~","",suggestions$suggestion)                  # remove the prefix AKTSUG~ from the suggestions
    suggestions <- suggestions %>% mutate(suggestion = regexp_replace(suggestion, "AKTSUG~", ""))
    #suggestions <- suggestions %>% pull(suggestion)
    flog.info("finish prepare suggestions")

    #trig <- grep("TRIG",suggestions$suggestion)                                          # identify the Trigger driven suggestions
    # remove triggers from subsequent analysis                                      
    #if(length(trig)>0) {
    #    #sugs <- suggestions[!(c(1:(dim(suggestions)[1])) %in% trig)]
    #    sugs <- suggestions %>% filter(!(c(1:sdf_dim(suggestions)[1])) %in% trig)
    #} else {
    #    sugs <- suggestions
    #}  
    sugs <- suggestions %>% filter(!like(suggestion, "%TRIG%"))                           # remove Triggers from suggestions
    
    #sugs[,feedbackDate:=as.Date(feedbackDate)]                                           # change field type
    sugs <- sugs %>% mutate(feedbackDate = as.Date(feedbackDate))

    #sugs[!(reaction %in% includeReactions),reaction:="Ignored"]                          # if reaction to suggestion is not in parameter includeReactions then set the reaction to IGNORE
    sugs <- sugs %>% mutate(reaction = ifelse(!(reaction %in% includeReactions), "Ignored", reaction))

    #sugs$recordId <- 1:nrow(sugs)
    #sugs <- mutate(sugs, recordId = 1 : sdf_dim(sugs)[1])                                # for subsequent processing number the records in the new suggestion table
    sugs <- sdf_with_unique_id(sugs, id="recordId")       # just need an id not need to be sequencetial, so use sdf_with_unique_id instead of sdf_with_sequential_id

    sugs <- sugs %>% mutate(dur = 0)
    
    #ign <- sugs[reaction=="Ignored"]                                                    # subset the ignored suggestions
    ign <- sugs %>% filter(reaction=="Ignored")

    if(sdf_dim(ign)[1] > 0)                                                              # if there are any ignored suggestions then find the time from suggestion to next action
    {
        # append the ignored suggestions to the intereactions table (now called events)
        ign <- sdf_bind_rows(ign, events)

        # sort ign table as setup to find time between suggestion and next action
        ign <- arrange(ign, repId, accountId, date)

        #ign[,dur:=c(NA,diff(date)),by=c("repId","accountId")]                           
        # add the dur field to the table that contains that difference in time
        ign <- ign %>% group_by(repId, accountId) %>%
                       mutate(date_lag = lag(date), dur = as.integer(datediff(date, date_lag))) %>% 
                       ungroup() %>% select (-date_lag)        
        
        #ign[,dur:=shift(dur,type="lead")]   
        # move the dur field values to the rows that contain the suggestion associated with the duration  
        ign <- ign %>% mutate(dur = lead(dur)) %>% 
                       mutate(dur = ifelse(is.na(dur), 0, dur))                                     

        #ign <- ign[reaction=="Ignored" & shift(reaction,type="lead")=="Touchpoint"]      # only include the appropriate types of differences
        ign <- ign %>% filter(reaction=="Ignored" & lead(reaction) == "Touchpoint")

        #sugs <- merge(sugs,ign[,c("recordId","dur"),with=F],by="recordId",all.x=T)       # merge the processed ignored table to the full suggestions table
        sugs <- sugs %>% left_join(select(ign, recordId, dur), by="recordId", suffix = c("_x", "_y"))
        sugs <- sugs %>% select(-dur_x) %>% dplyr::rename(dur = dur_y)

        #sugs <- sugs[is.na(dur),dur:=engageWindow+1]                                     # if there is no subsequent intereaction after a suggestionthan set the duration to one more than the engageWindow
        sugs <- mutate(sugs, dur = ifelse(is.na(dur), engageWindow+1, dur))

        sugs <- sugs %>% mutate(dur = ifelse(reaction!="Ignored", 0, dur))
    }

    #sugs[reaction!="Ignored",dur:=0]                                                     # if suggestions are not ignored then set the duration to 0
    # sugs <- sugs %>% mutate(dur, replace(dur, reaction!="Ignored", 0))
      
    #sugs$ctr <- 1                                                                        # setup to count the number of times that suggestions are engaged or not
    #sugs$include <- F                                                                    # finish that setup
    sugs <- sugs %>% mutate(ctr = 1, include = 0)

    #sugs[dur<=engageWindow,include:=T]                                                   # this is where the suggestions <= engageWindow are included
    #sugs <- mutate(sugs, include = replace(include, dur<=engageWindow, T))
    sugs <- mutate(sugs, include = ifelse(dur<=engageWindow, 1, 0))
    
    #sugs[, c("yes","total") := list(sum(include),sum(ctr)), by=c("repId","accountId")]       
    # count the number of suggestions offered and accepted
    sugs <- sugs %>% group_by(repId, accountId) %>% 
                     mutate(yes = sum(include,na.rm=TRUE), total = sum(ctr,na.rm=TRUE)) %>% ungroup() 


    #sugs[,probAccepted:=yes/total,by=c("repId","accountId")]                             # that ratio is the probability estimate                               
    sugs <- sugs %>% group_by(repId, accountId) %>% 
                     mutate(probAccepted = yes/total) %>% ungroup()

    #sugs[,k:=paste(repId,"_",accountId,sep="")]                                          # prepare for the rep x account non-suggestion engagement estimates
    sugs <- sugs %>% mutate(k = paste(repId, "_", accountId, sep=""))
    flog.info("finish prepare sugs")

    #events[,k:=paste(repId,"_",accountId,sep="")]                                        # prepare for the rep x account non-suggestion engagement estimates
    events <- events %>% mutate(k = paste(repId, "_", accountId, sep=""))

 ###### example of sugs at this point in code    
 #        recordId     type                                                   suggestion       date                                                 Title_vod__c repActionTypeId feedbackDate  reaction accountId repId dur
 #     1:        1 Call_vod                           a207764~cp9:VVVV~r2717~V~1:d:p1003 2017-02-20                             SUGGESTED ACTION: Detail  LYRICA               3   2017-02-22  Complete    207764  2717   0
 #     2:        2 Call_vod                              a207764~cp10:~r2717~V~1:d:p1003 2017-06-26                             SUGGESTED ACTION: Detail  LYRICA               3         <NA>   Ignored    207764  2717   1
 #     3:        3 Call_vod                     a209818~cp9:~r2717~V~1:d:p1003:mt1:m1213 2017-01-05 SUGGESTED ACTION: Detail LYR Lyrica DPN August 2015 POA v1.0               3         <NA>   Ignored    209818  2717   6
 #     4:        4 Call_vod                               a209818~cp9:~r2717~V~1:d:p1003 2017-02-01                             SUGGESTED ACTION: Detail  LYRICA               3   2017-03-25 Execution    209818  2717   0
 #     5:        5 Call_vod                              a209818~cp9:V~r2717~V~1:d:p1003 2017-02-15                             SUGGESTED ACTION: Detail  LYRICA               3   2017-02-16 Execution    209818  2717   0
 #    ---                                                                                                                                                                                                                  
 #618933:   618933 Call_vod          a300707~cp11:~r3683~V~1:d:p1001~2:d:p1003~3:d:p1014 2017-08-01            SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX               3         <NA>   Ignored    300707  3683  61
 #618934:   618934 Call_vod a47737~cp11:~r3683~V~1:d:p1001~2:d:p1003~3:d:p1009~4:d:p1014 2017-07-27   SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX, FLECTOR               3         <NA>   Ignored     47737  3683  61
 #618935:   618935 Call_vod         a189462~cp11:V~r3683~V~1:d:p1003~2:d:p1001~3:d:p1014 2017-08-01            SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX               3         <NA>   Ignored    189462  3683  61
 #618936:   618936 Call_vod        a192615~cp11:SV~r3683~V~1:d:p1003~2:d:p1001~3:d:p1014 2017-07-18            SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX               3         <NA>   Ignored    192615  3683  61
 #618937:   618937 Call_vod          a194741~cp11:~r3683~V~1:d:p1003~2:d:p1001~3:d:p1014 2017-07-25            SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX               3         <NA>   Ignored    194741  3683  61
 ####### end of example
    
    #acctReps <- unique(rbind(sugs[date>=sugStartDate,c("k","repId","accountId"),with=F], events[,c("k","repId","accountId"), with=F]) ) # pick out all unique account rep combinations
    sugs.ar <- sugs %>% filter(date >= sugStartDate) %>% select(k, repId, accountId)
    events.ar <- events %>% select(k, repId, accountId)
    acctReps <- sdf_bind_rows(sugs.ar, events.ar) %>% distinct()
    flog.info("finish prepare acctReps")

    #setkey(events,k,date)                                                                # sort events by rep account date
    events <- arrange(events, k, date)

    #events[,diff:=c(NA,diff(date)),by=c("repId","accountId")]                            # find interevent time differences by rep account
    # Hive functions work inside sparklyr mutate; dplyer or base R function may not work
    events <- events %>% group_by(repId, accountId) %>%
                         mutate(date_lag = lag(date), diff = as.integer(datediff(date, date_lag))) %>%
                         ungroup() %>%
                         select (-date_lag)
    flog.info("finish prepare events")
    
    flog.info("Now prepare for calculation of likelihood in parallel")
    likelihood <- calculateLikelihood(events, acctReps, today, numberCores)

    likelihood <- sdf_copy_to(sc, likelihood, "likelihood", overwrite=TRUE, memory=TRUE, repartition=48)

    flog.info("Now estimates the likelihood of an interaction by day of week and week of month.")

    #dayEstimate <- interactions[repActionTypeId==chl & date<=today] 
    dayEstimate <- interactions %>% filter(repActionTypeId==chl & date<=today)

    # estimates the likelihood of an interaction by day of week and week of month
    dayEstimate <- calculateDaysEstimate(dayEstimate, today, lookForward) 

    # add the probability that the suggestion was accepted estimated above to the likelihood estimates
    #likelihood <- merge(likelihood, unique(sugs[,c("repId","accountId","probAccepted"),with=F]), by=c("repId","accountId"), all=T) 
    likelihood <- likelihood %>% full_join(sugs %>% select(repId, accountId, probAccepted) %>% distinct(), by=c("repId","accountId"))

    #likelihood[is.na(probTouch),probTouch:=0]                                            # make sure missing estimates are zero
    likelihood <- likelihood %>% mutate(probTouch = ifelse(is.na(probTouch), 0, probTouch))

    #likelihood[probTouch<epsilon,probTouch:=0]                                           # make small estimates zero
    #likelihood <- likelihood[repId>0]   
    likelihood <- likelihood %>% mutate(probTouch = ifelse(probTouch<epsilon, 0, probTouch)) %>% filter(repId > 0)

    # prepare to spread the estimates over the lookForward interval
    likelihood <- addWeekdayWeekmonth(likelihood, today, lookForward)

    #likelihood <- merge(likelihood, dayEstimate, by=c("repId","accountId","mw.wd"), all.x=T)
    likelihood <- likelihood %>% left_join(dayEstimate, by=c("repId","accountId","mw_wd"))
    
    #likelihood[is.na(ratio),ratio:=0]
    likelihood <- likelihood %>% mutate(ratio = ifelse(is.na(ratio), 0, ratio))

    #likelihood[, PT:=ratio*probTouch]                                                 # finialize the separate estimates of prob touch, prob accept, and the combination     
    likelihood <- likelihood %>% mutate(PT = ratio * probTouch)

    #likelihood[is.na(probAccepted), probAccepted:=0]
    likelihood <- likelihood %>% mutate(probAccepted = ifelse(is.na(probAccepted), 0, probAccepted))

    #likelihood[, PA:=probAccepted]
    likelihood <- likelihood %>% mutate(PA = probAccepted)
    
    #likelihood[, PB:=(PA + PT*(1-PA))]
    likelihood <- likelihood %>% mutate(PB = (PA + PT*(1-PA)))

    flog.info("Get final likelihood.")
    
    tT <- copy(likelihood)                                                            # the next set of code is setup for saving results
    #tT$suggestionType <- "Target"
    tT <- tT %>% mutate(suggestionType = "Target")

    #setnames(tT,"PT","probability")
    tT <- dplyr::rename(tT, probability = PT)

    #tT[,c("PA","PB"):=NULL]
    tT <- tT %>% select(-c(PA, PB))

    tA <- copy(likelihood)
    #tA$suggestionType <- "Target"
    tA <- tA %>% mutate(suggestionType = "Target")

    #setnames(tA,"PA","probability")
    tA <- dplyr::rename(tA, probability = PA)

    #tA[,c("PT","PB"):=NULL]
    tA <- tA %>% select(-c(PT, PB))

    tB <- copy(likelihood)
    #tB$suggestionType <- "Target"
    tB <- tB %>% mutate(suggestionType = "Target")

    #setnames(tB,"PB","probability")
    tB <- dplyr::rename(tB, probability = PB)

    #tB[,c("PA","PT"):=NULL]
    tB <- tB %>% select(-c(PT, PA))

 ##    likelihood <- unique(rbind(tT,tA,tB,fill=T))

    if (useForProbability == "T")
        likelihood <- tT %>% distinct()
    else if (useForProbability == "A")
        likelihood <- tA %>% distinct()
    else
        likelihood <- tB %>% distinct()

    #likelihood$repActionTypeId <- chl
    #likelihood$runDate <- today+1
    likelihood <- likelihood %>% mutate(repActionTypeId = chl, runDate = date_add(today, 1))

    #likelihood[,c("learningRunUID","learningBuildUID","repUID","accountUID"):=list(RUN_UID,BUILD_UID,repId,accountId)]
    likelihood <- likelihood %>% mutate(learningRunUID = RUN_UID, learningBuildUID = BUILD_UID, repUID = repId, accountUID = accountId)

    likelihood <- likelihood %>% select(learningRunUID,learningBuildUID,repActionTypeId,repUID,accountUID,suggestionType,date,probability)
    
    flog.info("finish calculateEngagement")

    return(likelihood)
}

