#!/bin/bash
#
# aktana-learning Run script for running an R script in R learning module.
#
# description: Learning modules are written in R. The script is used
#              to run appropriate scripts in R packages
#
# created by : satya.dhanushkodi@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#

# make sure encoding is utf-8 to avoid issues with non-english character
export LC_ALL=en_US.UTF-8

# Set the learning home location if not set already
if [ -z "$LEARNING_HOME" ] ; then
   PRG="$0"
   LEARNING_HOME=`dirname "$PRG"`/..
fi

DATA_DIR=$LEARNING_HOME/../data
LIB_DIR=$LEARNING_HOME/../library

#while [ "$1" != "" ]; do
#    echo "Parameter $1"
#    shift
#done

while getopts m:r:h:s:j:u:p:c:d:e:b:g:i:o:t:l:f:v: name
do
        case $name in
          m)MODULE=$OPTARG;;
          r)RSCRIPT=$OPTARG;;
          h)DB_HOST=$OPTARG;;
          s)DB_NAME=$OPTARG;;
	  j)DB_NAME_CS=$OPTARG;;
          u)DB_USER=$OPTARG;;
          p)DB_PASSWORD=$OPTARG;;
          d)RUN_DATE=$OPTARG;;
          c)CUSTOMER=$OPTARG;;
          e)ENV_NAME=$OPTARG;;
          b)BUILD_UID=$OPTARG;;
          g)CONFIG_UID=$OPTARG;;
          i)RUN_UID=$OPTARG;;
          o)DEPLOY_ON_SUCCESS=$OPTARG;;
          t)DB_PORT=$OPTARG;;
          l)RUN_MODEL=$OPTARG;;
          f)HADOOP_LOG_DIR=$OPTARG;;
          v)RUN_DRIVER=$OPTARG;;
          *)echo "Invalid arg";;
        esac
done

# log dir (for use in BDP)
LOG_DIR=$LEARNING_HOME/../logs
if [ ! -z "$HADOOP_LOG_DIR" ] ; then
   LOG_DIR=$HADOOP_LOG_DIR
fi

# If module is not defined.
if [ -z "$MODULE" ] ; then
   echo "Error: MODULE (-m) is not defined correctly."
   exit 1
fi

# If rscript is not defined.
if [ -z "$RSCRIPT" ] ; then
   echo "Error: RSCRIPT (-r) is not defined correctly."
   exit 1
fi

# If dbhost is not defined.
if [ -z "$DB_HOST" ] ; then
   echo "Error: DB_HOST (-h) is not defined correctly."
   exit 1
fi

# If dbname is not defined.
if [ -z "$DB_NAME" ] ; then
   echo "Error: SCHEMA (DB_NAME) (-s) is not defined correctly."
   exit 1
fi

# If dbname_cs is not defined.
if [ -z "$DB_NAME_CS" ] ; then
   echo "Error: SCHEMA (DB_NAME_CS) (-J) is not defined correctly."
   exit 1
fi

# If dbuser is not defined.
if [ -z "$DB_USER" ] ; then
   echo "Error: DB_USER (-u) is not defined correctly."
   exit 1
fi

# If dbpassword is not defined.
if [ -z "$DB_PASSWORD" ] ; then
   echo "Error: DB_PASSWORD (-p) is not defined correctly."
   exit 1
fi

# If customer is not defined.
if [ -z "$CUSTOMER" ] ; then
   echo "Error: CUSTOMER (-c) is not defined correctly."
   exit 1
fi

# If rundate is not defined.
if [ -z "$RUN_DATE" ] ; then
   echo "Error: RUN_DATE (-d) is not defined correctly."
   exit 1
fi

# If BUILD_UID is not defined.
BUILD_UID_PARAM=''
if [ ! -z "$BUILD_UID" ] ; then
   BUILD_UID_PARAM='BUILD_UID="'"$BUILD_UID"'"'
fi

# If CONFIG_UID is not defined.
CONFIG_UID_PARAM=''
if [ ! -z "$CONFIG_UID" ] ; then
   CONFIG_UID_PARAM='CONFIG_UID="'"$CONFIG_UID"'"'
fi

# If RUN_UID is not defined.
if [ ! -z "$RUN_UID" ] ; then
   RUN_UID_PARAM='RUN_UID="'"$RUN_UID"'"'
fi

# If environment path is not defined.
if [ -z "$ENV_NAME" ] ; then
   ENV_NAME='default';
fi

# If DEPLOY_ON_SUCCESS is not defined.
DEPLOY_ON_SUCCESS_PARAM=''
if [ ! -z "$DEPLOY_ON_SUCCESS" ] ; then
  DEPLOY_ON_SUCCESS_PARAM='deployOnSuccess="'"$DEPLOY_ON_SUCCESS"'"'
fi

DB_PORT_PARAM=''
if [ ! -z "$DB_PORT" ] ; then
    DB_PORT_PARAM='port="'"$DB_PORT"'"'
fi

# if RUN_MODEL is not defined
RUN_MODEL_PARAM=''
if [ ! -z "$RUN_MODEL" ] ; then
  RUN_MODEL_PARAM='runmodel="'"$RUN_MODEL"'"'
fi

# if RUN_DRIVER is not defined
RUN_DRIVER_PARAM=''
if [ ! -z "$RUN_DRIVER" ] ; then
  RUN_DRIVER_PARAM='rundriver="'"$RUN_DRIVER"'"'
fi

CURRENTDT=$(date +%Y%m%d%H%M%S)
DB_HOST_PARAM='"'"$DB_HOST"'"'
DB_NAME_PARAM='"'"$DB_NAME"'"'
DB_NAME_CS_PARAM='"'"$DB_NAME_CS"'"'
DB_USER_PARAM='"'"$DB_USER"'"'
DB_PASSWORD_PARAM='"'"$DB_PASSWORD"'"'
RUN_DATE_PARAM='"'"$RUN_DATE"'"'
CUSTOMER_PARAM='"'"$CUSTOMER"'"'
ENVNAME_PARAM='"'"$ENV_NAME"'"'
RFULLSCRIPT="$RSCRIPT.r"
MODULE_FOR_LOG="${MODULE//\//.}"
LOGNAME="$MODULE_FOR_LOG.$RSCRIPT.$ENV_NAME.$CURRENTDT.stdout"
LOG_DIR_PARAM='"'"$LOG_DIR"'"'
DATA_DIR_PARAM='"'"$DATA_DIR"'"'
HOME_DIR_PARAM='"'"$LEARNING_HOME"'"'
LIB_DIR_PARAM='"'"$LIB_DIR"'"'

RCMD="R CMD BATCH --no-save --no-restore "
RFILES="$LEARNING_HOME/$MODULE/$RFULLSCRIPT $LOG_DIR/$LOGNAME"
RARGS="--args dbuser=$DB_USER_PARAM \
              dbpassword=$DB_PASSWORD_PARAM \
              dbhost=$DB_HOST_PARAM \
              dbname=$DB_NAME_PARAM \
	      dbname_cs=$DB_NAME_CS_PARAM \
	      $BUILD_UID_PARAM $CONFIG_UID_PARAM $RUN_UID_PARAM \
              rundate=$RUN_DATE_PARAM \
              libdir=$LIB_DIR_PARAM \
              logdir=$LOG_DIR_PARAM \
              datadir=$DATA_DIR_PARAM \
              homedir=$HOME_DIR_PARAM \
              envname=$ENVNAME_PARAM \
              customer=$CUSTOMER_PARAM \
              $DEPLOY_ON_SUCCESS_PARAM \
              $DB_PORT_PARAM \
              $RUN_MODEL_PARAM $RUN_DRIVER_PARAM"

RFULLCMD="$RCMD '$RARGS ' $RFILES"

# Make sure log directory exists
mkdir -p $LOG_DIR
mkdir -p $DATA_DIR

# Verify if the module is in place.
if [ ! -f "$LEARNING_HOME/$MODULE/$RFULLSCRIPT" ] ; then
        echo "Error: Could not find $LEARNING_HOME/$MODULE/$RFULLSCRIPT"
        exit 1
fi

echo "running $MODULE/$RFULLSCRIPT"

echo $RFULLCMD
eval $RFULLCMD

rc=$?
echo $rc

case $rc in
  0) echo "done running $MODULE/$RFULLSCRIPT";;
  *) echo "failed running $MODULE/$RFULLSCRIPT";;
esac

exit $rc
