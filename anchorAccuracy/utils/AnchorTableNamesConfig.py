import os
import rpy2.robjects as robjects
from common.pyUtils.logger import get_module_logger
from common.pyUtils.database_config import Singleton

script_path = os.path.realpath(__file__)
learning_dir = os.path.dirname(os.path.dirname(os.path.dirname(script_path)))
logger = get_module_logger(__name__)


@Singleton
class AnchorTableNamesConfig:
    """
    This is a singleton class holds the anchor input & output tables names configuration for current instance.
    """

    RepDateLocation_nightly = None
    RepDateFacility_nightly = None
    RepDateLocation_nightly_arc = None
    RepDateFacility_nightly_arc = None
    RepDateLocationAccuracy = None
    RepDateLocationAccuracy_agg = None

    def __reset_anchor_table_names_to_default(self):
        self.RepDateLocation_nightly = {"schema": "dse", "tableName": "RepDateLocation"}
        self.RepDateFacility_nightly = {"schema": "dse", "tableName": "RepDateFacility"}
        self.RepDateLocation_nightly_arc = {"schema": "stage", "tableName": "RepDateLocation_arc"}
        self.RepDateFacility_nightly_arc = {"schema": "stage", "tableName": "RepDateFacility_arc"}
        self.RepDateLocationAccuracy = {"schema": "learning", "tableName": "RepDateLocationAccuracy"}
        self.RepDateLocationAccuracy_agg = {"schema": "learning", "tableName": "AggRepDateLocationAccuracy"}

    def __update_config(self, paraRun_model):
        paraRunModel_config_file_path = "{}/dataAccessLayer/R/paraRun{}.R".format(learning_dir, paraRun_model.capitalize())
        if not os.path.exists(paraRunModel_config_file_path):
            logger.info("anchor tables names config file <{}> for model <{}> does not exists, use default table names".format(paraRunModel_config_file_path, paraRun_model))
            self.__reset_anchor_table_names_to_default()
        else:
            logger.info("Run anchorAccuracy for <{}> model with anchor table names configured in <{}>".format(paraRun_model,paraRunModel_config_file_path))
            # run r script to get anchorTables names configured in paraRun file
            anchor_table_config = self.__get_anchor_result_tables_config_from_r(paraRunModel_config_file_path, paraRun_model)
            self.__update_anchor_table_names_config(anchor_table_config)

    def __get_anchor_result_tables_config_from_r(self, paraRunModel_config_file_path, paraRun_model):
        robjects.r.source(paraRunModel_config_file_path)
        get_config_func_name = "dataAccessLayer.{}.paraRun.registerOutputTables".format(paraRun_model)
        get_config_func = robjects.globalenv[get_config_func_name]
        anchor_table_config_r = get_config_func()
        anchor_table_config = dict(zip(list(anchor_table_config_r.names), [{key: i.rx2(key)[0] for key in i.names} for i in anchor_table_config_r]))
        return anchor_table_config

    def __update_anchor_table_names_config(self, anchor_table_config):
        # get anchor result output table names from passed config
        self.RepDateLocation_nightly = anchor_table_config["RepDateLocation_nightly"] if "RepDateLocation_nightly" in anchor_table_config else None
        self.RepDateFacility_nightly = anchor_table_config["RepDateFacility_nightly"] if "RepDateFacility_nightly" in anchor_table_config else None
        # update archive table names based on the result tbales
        self.RepDateLocation_nightly_arc = {"schema": "learning", "tableName": "{}_arc".format(self.RepDateLocation_nightly["tableName"])} if self.RepDateLocation_nightly is not None else None
        self.RepDateFacility_nightly_arc = {"schema": "learning", "tableName": "{}_arc".format(self.RepDateFacility_nightly["tableName"])} if self.RepDateFacility_nightly is not None else None
        # update accuracy result save tables
        self.RepDateLocationAccuracy = {"schema": "learning", "tableName": "paraRun_RepDateLocationAccuracy_{}".format(self.RepDateLocation_nightly["tableName"].split("_")[-1])} if self.RepDateLocation_nightly is not None else None
        self.RepDateLocationAccuracy_agg = {"schema": "learning", "tableName": "paraRun_AggRepDateLocationAccuracy_{}".format(self.RepDateLocation_nightly["tableName"].split("_")[-1])} if self.RepDateLocation_nightly is not None else None

    def set_config(self, paraRun_model=None):

        if paraRun_model is None:
            logger.info("no anchor table names config file path passed, use default table names")
            self.__reset_anchor_table_names_to_default()
        else:
            self.__update_config(paraRun_model)
        logger.info("Set AnchorTableNamesConfig: {}".format(", ".join("{}={}".format(k,v) for k,v in self.__dict__.items())))
