
#
#
# aktana-learning model building code for Aktana Learning Engines.
#
# description: This is initiallization code
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#
##########################################################
##########################################################
#
# set up environment
#
##########################################################

cleanUp <- function () 
{
    CATALOGENTY <<- paste("Error RUN: ", CATALOGENTRY)
    exit()
    q('no',status=1)
}
