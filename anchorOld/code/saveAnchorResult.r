##########################################################
#
#
# aktana-learning Anchor estimates Aktana Learning Engines.
#
# description: save anchor results
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2015-10-27
#
# Copyright AKTANA (c) 2015.
#
#
####################################################################################################################

saveAnchorResult <- function(u,p,h,schema,port,result)
    {
        library(RMySQL)
        library(bizdays)

        flog.info("Save Anchor Result")

        drv <- dbDriver("MySQL")
        con <- dbConnect(drv,user=u,password=p,host=h,dbname=schema,port=port)

        result$latitude <- as.numeric(result$latitude)
        result$longitude <- as.numeric(result$longitude)
    
        cal <- create.calendar(name="USA/ANBIMA", holidays=holidaysANBIMA, weekdays=c("saturday", "sunday"))
        result[,date:=offset(Sys.Date(),offset+1,cal)]
        result$createdAt <- Sys.time()
        result$offset <- NULL

       # delete old data
       dbGetQuery(con,"TRUNCATE TABLE RepDateLocation")
        
       dbWriteTable(con, name="RepDateLocation", value = as.data.frame(result),
                   overwrite = FALSE, append=TRUE, row.names=FALSE, field.types=list(repId="int(11)",date="date",createdAt="datetime",latitude="double",longitude="double",method="character(64)") )

        dbDisconnect(con)
    }

