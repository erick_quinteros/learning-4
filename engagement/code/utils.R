####################################################################################################################
#
#
# aktana- engagement estimates Aktana Learning Engines.
#
# description: helper functions.  
#
# created by : wendong.zhu@aktana.com
#
# created on : 2018-10-26
#
# Copyright AKTANA (c) 2018.
#
#
####################################################################################################################

# Function to caldulate ratio for each weekday (with weak of month) in order to
#    allocate probabilities
#
# Input: dayEstimate - dataframe or data.table of probabilities
#        today - today's date
#        loofForward - predicted number of days for future
# Output: dayEstimate
#
calculateDaysEstimate <- function(dayEstimate, today, lookForward)
{
    library(data.table)

    setkey(dayEstimate,accountId,repId,date)   # the estimates are obtained by simple counting for each rep-account and then taking the ratio - multinomial approach

    dayEstimate <- unique(dayEstimate, by=c("accountId", "repId", "date"))
    dayEstimate[, c("monthday", "weekday") := list(as.numeric(format(date,"%d")), as.factor(weekdays(date)))]
    dayEstimate[, monthweek := ceiling(monthday/7)]
    dayEstimate[, mw.wd := paste0(monthweek, "_", weekday)]
    dayEstimate <- dayEstimate[, c("repId", "accountId", "mw.wd"), with=F]
    dayEstimate$ctr <- 1
    dayEstimate[, tot := sum(ctr), by = c("repId", "accountId", "mw.wd")]

    setkey(dayEstimate, repId, accountId, mw.wd)

    days <- paste0(ceiling(as.numeric(format(today+(1:lookForward), "%d"))/7), "_", weekdays(today+(1:lookForward)))
    dayEstimate <- dayEstimate[mw.wd %in% days]
    dayEstimate <- unique(dayEstimate)
    dayEstimate[, total := sum(tot), by=c("repId","accountId")]
    dayEstimate[, ratio := tot/total]             # the estimates are saved in the dayEstimate table

    return (dayEstimate)
}

# Function to add weekday and monthday to probability dataframe
#
# Input: probs - dataframe or data.table of probabilities
#        today - today's date
#        loofForward - predicted number of days for future
# Output: probs
#
addWeekdayWeekmonth <- function(probs, today, lookForward)
{
    tmp <- NULL
    for(dte in 1:lookForward)         # prepare to spread the estimates over the lookForward interval
    {
        probs$date <- today + dte     # replicate the estimate for each lookForward day
        tmp <- rbind(probs, tmp)
    }
    probs <- tmp

    # use the day of week & week of month estimates for distributing weights over the next lookForward days
    probs[, c("monthday", "weekday") := list(as.numeric(format(date,"%d")), as.factor(weekdays(date)))] 
    probs[, monthweek := ceiling(monthday/7)]
    probs[, mw.wd := paste0(monthweek,"_",weekday)]

    return (probs)
}
