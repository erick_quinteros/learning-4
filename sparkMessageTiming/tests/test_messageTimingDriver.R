context('testing sparkMessageTimingDriver script in TTE module')
print(Sys.time())

# load packages and scripts required to run tests
library(openxlsx)
library(Learning)
library(uuid)
library(data.table)
library(sparkLearning)
library(sparklyr)
library(dplyr)
library(openssl)

source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))

libdir <<- sprintf("%s/library", homedir)

# writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
requiredMockDataList <- list(pfizerusdev=c('Product','RepTeam','Message','MessageSetMessage','Event','EventType','Interaction','InteractionType','ProductInteractionType','InteractionProduct','InteractionAccount','InteractionProductMessage','AccountProduct','Rep','RepTeamRep','RepAccountAssignment','Account','MessageSet', 'RepActionType', 'AccountTimeToEngage'), pfizerusdev_learning=c('LearningFile','LearningBuild','LearningRun','AccountTimeToEngageReport','SegmentTimeToEngageReport','TTEhcpReport','TTEsegmentReport','AKT_Message_Topic_Email_Learned','Sent_Email_vod__c_arc'), pfizerprod_cs=c('Approved_Document_vod__c','RecordType', 'Call2_Sample_vod__c'))

# LearningRun is for testing, not required for run messageSequenceDriver
resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList)

# get buildUID
BUILD_UID <<- readModuleConfig(homedir, 'messageTiming','buildUID')
# add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
setupMockBuildDir(homedir, 'messageTiming', BUILD_UID, files_to_copy='learning.properties')

# add entry to learningBuild (simulate what done by API before calling the R script)
config <- initializeConfigurationNew(homedir, BUILD_UID)
VERSION_UID <- config[["versionUID"]]
CONFIG_UID <- config[["configUID"]]

con <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
SQL <- sprintf("INSERT INTO LearningBuild (learningBuildUID,learningVersionUID,learningConfigUID,isDeployed,executionStatus,executionDatetime,isDeleted) VALUES('%s','%s','%s',0,'running','%s',0);",BUILD_UID, VERSION_UID, CONFIG_UID, now)
dbClearResult(dbSendQuery(con,SQL))
dbDisconnect(con)

sc <<- initializeSpark(homedir, master="local", version="2.3.1")

# run script
if(!exists("RUN_UID")) RUN_UID <<- UUIDgenerate()

print('start running sparkMessageTimingDriver')
source(sprintf('%s/sparkMessageTiming/messageTimingDriver.r', homedir))

# test cases
# test file structure
test_that("build dir has correct struture", {
  expect_file_exists(sprintf('%s/builds/',homedir))
  expect_file_exists(sprintf('%s/builds/%s',homedir,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/learning.properties',homedir,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/log_%s.txt',homedir,BUILD_UID,paste("Build",RUN_UID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/print_%s.txt',homedir,BUILD_UID,paste("Build",RUN_UID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/models_A_%s',homedir,BUILD_UID,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/messageTimingDriver.r_%s.xlsx',homedir,BUILD_UID,BUILD_UID))
  expect_num_of_file(sprintf('%s/builds/%s/models_A_%s',homedir,BUILD_UID,BUILD_UID),1)
})

test_that("model reference saved in excel is correct", {
  # read excel sheet 2 - importance file
  importance <- read.xlsx(sprintf('%s/builds/%s/messageTimingDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=1)
  expect_equal(dim(importance), c(20, 2))
  expect_equal(c("preT","preS","preV","numS","numV") %in% importance$names, c(TRUE,TRUE,TRUE,TRUE,TRUE))

  # read excel sheet 3 - accuracy file
  models <- read.xlsx(sprintf('%s/builds/%s/messageTimingDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=2)
  expect_equal(dim(models), c(1, 5))
#  expect_equal(models$expiredStatus, c(FALSE,TRUE))
#  expect_equal(models$messageName, c("Kentucky RTE","Chantix Side Effects RTE"))

  for (file in models$modelName)
  {expect_file_exists(file)}

})

# test entry in DB is fine
test_that("correct entry write to learning DB", {
  con <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  learningRun <- dbGetQuery(con, "SELECT * from LearningRun;")
  SQL <- sprintf("SELECT * from LearningBuild where learningBuildUID='%s';",BUILD_UID)
  learningBuild <- dbGetQuery(con, SQL)
  SQL <- sprintf("SELECT * from LearningFile where learningBuildUID='%s';",BUILD_UID)
  learningFile <- dbGetQuery(con, SQL)
  dbDisconnect(con)
  expect_equal(dim(learningRun),c(0,10))
  expect_equal(dim(learningBuild), c(1,9))
  expect_equal(unname(unlist(learningBuild[,c('learningVersionUID','learningConfigUID','isDeployed','executionStatus','isDeleted')])), c(VERSION_UID,CONFIG_UID,0,'success',0))
  expect_equal(learningBuild$updatedAt>=learningBuild$createdAt, TRUE)
  expect_equal(dim(learningFile), c(4,9))
  expect_setequal(learningFile$fileName, c("accuracy", "importance", "log", "print"))
})

# run test cases on log file contents
# read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
log_contents <- readLogFile(homedir,BUILD_UID,paste("Build",RUN_UID,sep="_"))

#test_that("check log file general structure", {
#  ind <- grep('Number of targets:',log_contents)
#  expect_str_end_match(log_contents[ind],'5')
#  expect_str_end_match(log_contents[ind+1],'CHANTIX')
#})

#test_that("check processing AccountProduct/Account predictors", {
#  ind <- grep('Analyzing account product data',log_contents)
#  expect_length(ind,1)
#  expect_str_start_match(log_contents[ind+1],'151 of 204')  # number of predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique value
#  expect_str_start_match(log_contents[ind+2],'1 of 21 numeric predictors (53 all predictors)')  # number of numeric predictors from AccountProduct/Account dropped before loop to build models as there are either less than 5 unique values or having <2 quantile >0
#  expect_str_start_match(log_contents[ind+3],'0 of 52')  # number of remaining predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique values (2nd check after processing numeric and character variable separately
#  expect_str_end_match(log_contents[ind+4],'52 to 533')  # check for dcast manupilation
#  expect_str_end_match(log_contents[ind+5],'added 72 and becomes 605') # check EmailTopic Open Rates predictor added
#})

#test_that("check message a3RA00000001MtAMAU have the correct design matrix", {
#  ind <- grep('OPEN...a3RA00000001MtAMAU',log_contents)
#  expect_length(ind,1)
#  expect_str_end_match(log_contents[ind+1],'29')  # number of TargetName records
#  expect_str_end_match(log_contents[ind+2],'319')  # number of SendName records
#  expect_str_end_match(log_contents[ind+4],'222') # Number of accounts considered for dynamic features
#  expect_str_end_match(log_contents[ind+6],'(227,34)')  # dim of predictors from dynamic features to static features
#  expect_str_end_match(log_contents[ind+7],'(315,638)')  # dimension of start design matrix
#  expect_str_end_match(log_contents[ind+9],'20')  # number of positive target records
#  expect_str_end_match(log_contents[ind+10],'(227,638)')  # dimension of design matrix with positive send records
#  expect_str_start_match(log_contents[ind+11],'133 of 638')  # number of predictors from design matrix allModel(static + dynamic features) dropped in target loop as they have less than 2 unique values after subsetting with send and filling NA with 0
#  expect_str_start_match(log_contents[ind+12],'3 of 505')  # number of remaining predictors in the design matrix dropped because of learning.properties config setting
#  expect_str_end_match(log_contents[ind+13],'(227,502)')  # final design matrix dimension
#})


# ###################  test for the case build fail #########################################
# rm(BUILD_UID)
# BUILD_UID <<- readModuleConfig(homedir, 'messageSequence','buildUID_fail')
# # not copy learning properties file so that it will file
# buildUIDDirpath <- paste(homedir,'/builds/',BUILD_UID,sep='')
# if (! dir.exists(buildUIDDirpath)) { # generate buildUID folder
#   dir.create(buildUIDDirpath)
# } else { # clean buildUID folder
#   unlink(paste(buildUIDDirpath,'/*',sep=''),recursive = TRUE, force = TRUE)
# }
# # insert into learningBUild to prepare for run
# drv <- dbDriver("MySQL")
# con <- dbConnect(drv,user=dbuser,host=dbhost,dbname=dbname_learning,port=port,password=dbpassword)
# now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
# SQL <- sprintf("INSERT INTO LearningBuild (learningBuildUID,learningVersionUID,learningConfigUID,isDeployed,executionStatus,executionDatetime,isDeleted) VALUES('%s','%s','%s',0,'running','%s',0);",BUILD_UID, VERSION_UID, CONFIG_UID, now)
# dbClearResult(dbSendQuery(con,SQL))
# dbDisconnect(con)
# # run script
# RUN_UID <<- UUIDgenerate()
#
# print('start running messageSequenceDriver with failure expectation')
# expect_error(source(sprintf('%s/messageSequence/messageSequenceDriver.r',homedir)))
#
# # test entry in DB is fine
# test_that("correct entry write to learning DB", {
#   drv <- dbDriver("MySQL")
#   con <- dbConnect(drv,user=dbuser,host=dbhost,dbname=dbname_learning,port=port,password=dbpassword)
#   SQL <- sprintf("SELECT * from LearningBuild WHERE learningBuildUID ='%s';",BUILD_UID)
#   learningBuild <- dbGetQuery(con, SQL)
#   SQL <- sprintf("SELECT * from LearningFile WHERE learningBuildUID = '%s';",BUILD_UID)
#   learningFile <- dbGetQuery(con, SQL)
#   print(learningFile)
#   dbDisconnect(con)
#   expect_equal(dim(learningBuild), c(1,9))
#   expect_equal(learningBuild$executionStatus, 'failure')
#   expect_equal(dim(learningFile), c(2,9))
#   expect_equal(sort(learningFile$fileName), c("log", "print"))
# })
#
# # run test cases on log file contents
# test_that("check log file contents", {
#   log_file_path <- sprintf('%s/builds/%s/log_%s.txt',homedir,BUILD_UID,RUN_UID)
#   ff <- file(log_file_path,'r')
#   log_contents <- readLines(ff, warn=FALSE)
#   close(ff)
#   expect_length(log_contents, 2)
# })
